/* Se agregan las librerias necesarias para el control de procesos, 
E/S y funciones de espera */
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

using namespace std;

/* Clase "Fork" posee atributos privados y metodos publicos necesarios para el control 
de los procesos y subprocesos en el programa*/
class Fork {
  /*atributos privados:
    pid (process id)
    entero "segundos": cantidad de segundos de espera entre proceso padre e hijo
    cadena "URL": dirrecciòn o link del video a descargar */
  private:
    pid_t pid;
  int segundos;
  char * url;

  public:
    /* Constructor, recibe los segundos y la URL */
    Fork(int seg, char * download) {
      segundos = seg;
      url = download;
      /* Se llama a los parametros para los procesos */
      crear();
    }

  /* Mètodos */
  void crear() {
    /* Se utiliza "fork" para crear el proceso hijo, este retorna un nùmero identificador de ser exitoso */
    pid = fork();
  }

  void procesoPadre() {

    cout << "Ejecutando código proceso padre: " << getpid() << endl;
    /* Se utiliza sox para reproducir en consola el audio descargado, -v es para el volumen */
    sleep(2);
    execlp("sox", "play", "-v", "1", "song.mp3", NULL);
    cout << "Finalizò proceso padre" << endl;
  }

  void procesoHijo() {
    /* Se da paso a la ejecucion del proceso hijo, se le informa al usuario */
    cout << "Ejecutando proceso hijo, Extrae audio ID:" << getpid() << endl;

    /* Se extrae el audio segùn formato de youtube.dl, este se renombra 
	  para su posterior uso y se le da la extenciòn .mp3 */

    execlp("youtube-dl", "youtube-dl", "-x", "--audio-format",
      "mp3", "-o", "song.%(ext)s", url, NULL);

    sleep(segundos);
    /* Se informa al usuario de la finalizaciòn del subproceso */
    cout << "Finaliza proceso hijo" << endl;
  }

  void ejecutarCodigo() {
    /* El id del proceso nos permitirà validarlo 
	   de ser menor a 0 entonces serà invalido, por lo tanto no se pudo crear */
    if (pid < 0) {

      cout << "Proceso invalido, no se ha podido crear" << endl;
      /* De ser igual a 0 se llama nuevamente */
    } else if (pid == 0) {

      procesoHijo();
      /* De lo contrario se espera a que el proceso finalice y se regresa al proceso padre */
    } else {

      wait(NULL);

      procesoPadre();
    }
  }
};

/* Funcion pincipal del programa */
int main(int argc, char * argv[]) {
  /* Cadena "URL" paràmetro de entrada es la direccion o link de youtube a descargar */
  char * url = argv[1];

  /* De no ser NULL implica que el usuario ingresò algun valor */
  if (url != NULL) {
    /* Se llama a la clase y se genera su instancia */
    Fork fork(3, url);

    /* Se llama al mètodo que ejecuta el codigo*/
    fork.ejecutarCodigo();
    /* De ser NULL el paràmetro de entrada se le informa al usuario y se cierra el programa*/
  } else {
    cout << "Error, no ingresò una URL, intentelo nuevamente" << endl;
  }

  return 0;
}
