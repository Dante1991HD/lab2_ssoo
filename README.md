# Extracción y reproducción audio de YouTube desde terminal Linux (Ubuntu/Debian)

## Lab 2 - Unidad I - SSOO y redes

## Descripcòn del programa
El presente programa presenta una serie de procesos que permiten la descarga de un video de youtube, se requiere de una URL de youtube, ademàs de la instalaciòn de otros tres programas, ffplay, youtube-dl y sox. Los procesos son creados con la función **fork()** de C++, el cual permite un proceso hijo descargue un video y extraiga el audio, mientras que su proceso padre reproduce dicha extracción. 

## Prerrequisitos

### Sistema operativo

* Sistema operativo con el kernel de Linux, Ubuntu o Debian preferentemente

Siempre para instalar o correr programas se recomienda actualizar su máquina mediante el comando desde la terminal 
```
sudo apt update && sudo apt upgrade
```
### Make

Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

### Youtube-dl 

* Para la ejecuciòn del programa se requiere youtube-dl, la siguente es una guia de instalaciòn

**Usando curl**
```
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
```
* Se le otorgan permisos
```
sudo chmod a+rx /usr/local/bin/youtube-dl
```
De igual manera se puede instalar el programa curl mediante el siguiente comando `sudo apt  install curl`. Presenta diversas versiones por lo que para revisar la versión instalada se puede ocupar `curl --version`.

**Sin curl**

En el caso de no tener curl se puede instalar youtube-dl con la siguiente serie de comandos:
```
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
```
* Se le otorgan permisos
```
sudo chmod a+rx /usr/local/bin/youtube-dl
```
* Mayor informaciòn http://rg3.github.io/youtube-dl/download.html

**Para extraer audio**

Para las funciones de youtube-dl de extracción de audio se necesitan ciertos paquetes los cuales el programa sugiere a la hora de instalar: _ffmpeg/avconv and ffprobe/avprobe_. Estos se instalan bajo el siguiente comando: 
```
sudo apt-get install ffmpeg
```
### SOX

Para que posterior a la extracción del audio este se pueda reproducir desde la misma terminal de manera automática se necesita del programa SOX, el cual debe ser instalado de la siguiente manera:
```
sudo apt install sox
```
Para poder reproducir todo tipo de archivos de audio se necesita instalar una librería para sox con el siguiente comando: 
```
sudo apt-get install libsox-fmt-all
```
* Mayor informacion https://ubunlog.com/sox-reproduce-mp3-terminal/#Reproducir_archivos_mp3_usando_Sox


## Compilación y ejecución del programa

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:
```
make
```
** De falar make

* En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:
```
g++ programa.cpp -o programa
```
### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando con parámetro de entrada:
```
./programa {URL}
```
## Salidas
* Ejemplo ejecuciòn:
```
./programa https://www.youtube.com/watch?v=dQw4w9WgXcQ
```
```
jecutando proceso hijo, Extrae audio ID:28059
[youtube] dQw4w9WgXcQ: Downloading webpage
[download] Destination: song.webm
[download]   0.0% of 3.28MiB at  8.40KiB/s
[download]   0.1% of 3.28MiB at 25.18KiB/s
[download]   0.2% of 3.28MiB at 58.66KiB/s
[download]   0.4% of 3.28MiB at 125.46KiB/
[download]   0.9% of 3.28MiB at 108.19KiB/
[download]   1.9% of 3.28MiB at 91.46KiB/s
ETA 00:36
```
```
[download] 100% of 3.28MiB in 00:40
[ffmpeg] Destination: song.mp3
Deleting original file song.webm (pass -k to keep)
Ejecutando código proceso padre: 28187

song.mp3:

 File Size: 3.73M     Bit Rate: 141k
  Encoding: MPEG audio    
  Channels: 2 @ 16-bit   
Samplerate: 48000Hz      
Replaygain: off         
  Duration: 00:03:32.09  

In:0.00% 00:00:00.00 [00:03:32.09] Out:0  In:0.16% 00:00:00.34 [00:03:31.75] Out:16.In:0.20% 00:00:00.43 [00:03:31.66] Out:20.In:0.28% 00:00:00.60 [00:03:31.49] Out:28.In:0.36% 00:00:00.77 [00:03:31.32] Out:36.In:0.44% 00:00:00.94 [00:03:31.15] Out:45.In:0.48% 00:00:01.02 [00:03:31.06] Out:49.In:0.56% 00:00:01.19 [00:03:30.89] Out:57.In:0.64% 00:00:01.37 [00:03:30.72] Out:65.In:0.72% 00:00:01.54 [00:03:30.55] Out:73.7k [-=====|=====-] Hd:2.5 Clip:0    
Aborted.
```
La salida forzosa del programa en cualquier parte de su ejecución es mediante 
```
ctl+z 
```
o 
```
ctl+c
```
## Construido con
* Lenguaje c++: librería iostream, unistd.h y sys/wait.h

## Autor
- Dante Aguirre Solis 
- daguirre12@alumnos.utalca.cl

